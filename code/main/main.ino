/*
 * Kreimer Alexander 5BHEL HTL Hollabrunn 2019/20
 * LED Control for an Infinity Mirror
 * 
 * Filename:  main.ino
 * Startdate: 30.06.2019
 * 
 * Description:
 *  Arduino program, which reads an IR-diodes Input on Digital Pin 2 using external Interrupts and the IRremote library.
 *  The program then controls an WS2812 adressable LED strip accordingly using the FastLED library.
 *  This program is used in an Infinity Mirror project.
 *  
 * Required/Used Hardware:
 *  -Arduino Uno
 *  -IR Sensor
 *  -WS2812 LED strip
 *  -Breadboard
 *  -5V Powersupply for the LED strip
 *  
 * Known Bugs:
 *  -Program crashed after a short time in Fade Mode
 *  -Blink Programm doesnt Blink properly (move FastLED.show() down a line) (fix not necessary)
 *  
 * ToDo:
 *  -fix Fade
 *  -add Rainbow mode?
 *  -add 2 more colors (button 8 and 9)
 */
#include <FastLED.h>
#include <IRremote.h>

#define LED_DT 9          //Pin der LED strip
#define IR_PIN 2          //Pin der IR diode   Pin 2 = Externes Interrupt möglich
#define COLOR_ORDER GRB   //Farbreihenfolge des LED Streifens
#define LED_TYPE WS2812   //LED-Streifen-Typ
#define NUM_LEDS 100      //Anzahl der anzusteuernden LEDs
int TIME=200;             //Delaywert
int MODE=1;               //Modes (Hold=0, Run=1)
int Gr=255, Ro=0, Bl=0;   //Variablen für RGB Werte

uint8_t bright = 60;      //Variable für Helligkeit

struct CRGB leds[NUM_LEDS]; //Strukur zum Setzen der leds

IRrecv irrecv(IR_PIN);
decode_results results;

void setup(){
  LEDS.addLeds<LED_TYPE, LED_DT, COLOR_ORDER>(leds,NUM_LEDS); //LED-Streifen wird initialisiert
  FastLED.setBrightness(bright);                              //Helligkeit einstellen         
  attachInterrupt(0,CHECK_IR,CHANGE);                         //Interrupt wird initialisiert. Interrupt wenn sich der Wert am Pin 2 ändert. CHECK_IR wird aufgerufen.
  irrecv.enableIRIn();                                        //IR wird enabled
  Serial.begin(9600);                                         //Serielle Datenübertragung festgelegt (Baudrate)
}

void loop() {
  static int i=0;                   //Variable für den Durchlaufen Modus      
  if(MODE==1){                      //Wenn Ch gedrückt wurde.    Lässt eine Farbe durchlaufen
    if(i>=NUM_LEDS){                //Wenn das Ende des LED-Streifens erreicht wird, springt das Programm zurück zum Anfang
        i=0;
      }
    leds[i].setRGB(Gr,Ro,Bl);       //eine (1) LED wird eingeschalten
    FastLED.show();                 
    delay(TIME);                    
    leds[i].setRGB(0,0,0);          //dieselbe LED wird wieder ausgeschalten, und danach die nächste eingeschalten
    FastLED.show();
    i++;
  }   
  else if(MODE==2){                 //Wenn Ch+ gedrückt wurde 
    for(i=0;i<NUM_LEDS;i++){
      leds[i].setRGB(Gr,Ro,Bl);     //LEDs werden eingeschaltet
      
    }FastLED.show();
    delay(TIME);
    for(i=0;i<NUM_LEDS;i++){        //LEDs werden wieder ausgeschaltet
      leds[i].setRGB(0,0,0);
      
    }FastLED.show();
    delay(TIME);
 }
 else if(MODE==3){
    FADE();
 }
}

void CHECK_IR(){                      //Liest die IR-Diode ein
  while(irrecv.decode(&results)){     //Taste wird eingelesen
  Serial.println(results.value, HEX); //Hexwert wird in die Konsole ausgegeben
  int value=results.value;              
    switch(value){                    //Switch mit dem eingelesen Wert
      case 0xFFA25D:                  //CH- Taste : Hold-Modus
          MODE=0;                     //MODE Variable wird auf 0 gesetzt (Hold Modus)
          HOLD();                     //Funktion HOLD wird aufgerufen
          break;
      case 0xFF629D:                  //Ch Taste : Rennen-Modus
          MODE=1;                     //MODE Variable wird auf 1 gesetzt (Run Modus)
          break;
      case 0xFFE21D:                  //Ch+ Taste : Blink-Modus
          MODE=2;                     //MODE Variable wird auf 2 gesetzt (Blink Modus)
          break;   
      case 0xFFC23D:                  //Pause Taste : Fade
          MODE=3;                     //MODE Variable wird auf 3 gesetzt (Fade Modus)
          break;
      case 0xFF6897:                  //0 leds werden abgeschalten
          Gr=0;
          Bl=0;
          Ro=0;
          HOLD();
          break;
      case 0xFF30CF:                  //1 Blau
          Bl=255;
          Ro=0;
          Gr=0;
          if(MODE==0){                 //Siehe *
            HOLD();
          }
          break;
      case 0xFF18E7:                  //2 Rot
          Ro=255;
          Bl=0;
          Gr=0;
          if(MODE==0){                //Siehe *
            HOLD();
          }
          break;
      case 0xFF7A85:                  //3. Grün
          Gr=255;
          Ro=0;
          Bl=0;
          if(MODE==0){                //Siehe *
            HOLD();
          }
          break; 
      case 0xFF10EF:                  //4 Weiß
          Gr=255;
          Bl=255;
          Ro=255;
          if(MODE==0){                //Siehe *
            HOLD();
          }
          break;
      case 0xFF38C7:                  //5 Magenta
          Gr=255;
          Ro=0;
          Bl=255;
          if(MODE==0){                //Siehe *
            HOLD();
          }
          break;
      case 0xFF5AA5:                  //6  Gold
          Gr=215;
          Ro=255;
          Bl=0;
          if(MODE==0){                //Siehe *
            HOLD();
          }
          break;
      case 0xFF42BD:                  //7 Warm White
          Gr=172;
          Ro=255;
          Bl=68;
          if(MODE==0){                //Siehe *
            HOLD();
          }
          break;
      case 0xFF4AB5:                  //8 NC
          Gr=0;
          Ro=0;
          Bl=128;
          if(MODE==0){                //Siehe *
            HOLD();
          }
          break;     
      case 0xFF52AD:                  //9 NC
          Gr=128;
          Ro=128;
          Bl=0;
          if(MODE==0){                //Siehe *
            HOLD();
          }
          break;            
      case 0xFFE01F:                  //- Taste   Helligkeit verringern
          bright=bright-10;
          if(bright<=0){
            bright=0;
          }
          FastLED.setBrightness(bright);
          FastLED.show();
          break;   
      case 0xFFA857:                  //+ Taste Helligkeit erhöhen
          bright=bright+10;
          if(bright>=240){
            bright=240;
          }
          FastLED.setBrightness(bright);
          FastLED.show();
          break;   
      case 0xFF02FD:                  //Next Taste  Delayzeit erhöhen
          TIME+=50;
          if(TIME>=2000){
            TIME=2000;
          }
          break;
      case 0xFF22DD:                  //Prev Taste Delayzeit verringern
          TIME-=25;
          if(TIME<=25){
            TIME=25;
          }
          break;   
    }
  irrecv.resume();
  }
}

void HOLD(){                           //Funktion, welche alle LEDs in einer bestimmten Farbe einschaltet                                    //Wird immer aufgerufen, wenn die Farbe geändert wird
    for(int j=0;j<=NUM_LEDS;j++){        //Zählschleife, um alle LEDs durchzugehen
      leds[j].setRGB(Gr,Ro,Bl);
    }
    FastLED.show();
}

void FADE(){
    int fade_brightness = 0;
    for(fade_brightness=0;fade_brightness<=255;fade_brightness++){
        FastLED.setBrightness(fade_brightness);
        FastLED.show();
        delay(TIME/20);
    }
    for(fade_brightness=255;fade_brightness>=0;fade_brightness--){
        FastLED.setBrightness(fade_brightness);
        FastLED.show();
        delay(TIME/20);
    } 
}
